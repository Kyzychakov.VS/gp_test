create or replace function gp_test.update_data() returns trigger as $update_data$
begin 
	new.last_date := current_timestamp;
	new.last_user := current_user;
	return new;
end;
$update_data$ language plpgsql;