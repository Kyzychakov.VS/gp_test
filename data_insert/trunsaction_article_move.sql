insert into gp_test.article_mov(code, material, material_name, batch, factory,
warehouse, type_moving, type_product, material_doc, posit_product,
date_moving, sum_product, count_product, type_count_product,
main_tool, link_product, article_key)
select *,
md5(concat(code, material, material_name, factory, type_moving, material_doc))::uuid article_key
  from tmp.article_mov m
 on conflict (article_key) do update set
code = excluded.code,
material = excluded.material,
material_name = excluded.material_name,
batch = excluded.batch,
factory = excluded.factory,
warehouse = excluded.warehouse,
type_moving = excluded.type_moving,
type_product = excluded.type_product,
material_doc = excluded.material_doc,
posit_product = excluded.posit_product,
date_moving = excluded.date_moving,
sum_product = excluded.sum_product,
count_product = excluded.count_product,
type_count_product = excluded.type_count_product,
main_tool = excluded.main_tool,
link_product = excluded.link_product;

truncate tmp.article_mov;