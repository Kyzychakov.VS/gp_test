-- основная таблица
CREATE TABLE IF NOT EXISTS gp_test.article_mov (
	code int4 NOT NULL,
	material varchar(50) NOT NULL,
	material_name varchar(300) NOT NULL,
	batch varchar(50) NULL,
	factory int4 NOT NULL,
	warehouse varchar(20) NULL,
	type_moving int4 NOT NULL,
	type_product varchar(10) NOT NULL,
	material_doc varchar(50) NOT NULL,
	posit_product varchar(10) NOT NULL,
	date_moving timestamp NOT NULL,
	sum_product numeric(10, 2) NOT NULL,
	count_product numeric(10, 2) NOT NULL,
	type_count_product varchar(10) NOT NULL,
	main_tool varchar(100) NULL,
	link_product varchar(100) NOT NULL,
	article_key uuid NOT NULL,
	last_date timestamp NOT NULL DEFAULT now(),
	last_user varchar NULL
);

CREATE UNIQUE INDEX IF NOT EXISTS article_mov_article_key_idx ON gp_test.article_mov USING btree (article_key);

create or replace trigger update_data before insert or update on gp_test.article_mov
for each row execute function gp_test.update_data();

-- в схеме tmp
CREATE TABLE IF NOT EXISTS tmp.article_mov(
	code int4 NOT NULL,
	material varchar(50) NOT NULL,
	material_name varchar(300) NOT NULL,
	batch varchar(50) null,
	factory int4 NOT NULL,
	warehouse varchar(20) NULL,
	type_moving int4 NOT NULL,
	type_product varchar(10) NOT NULL,
	material_doc varchar(50) NOT NULL,
	posit_product varchar(10) NOT NULL,
	date_moving timestamp NOT NULL,
	sum_product numeric(10, 2) NOT NULL,
	count_product numeric(10, 2) NOT NULL,
	type_count_product varchar(10) not null,
	main_tool varchar(100) null,
	link_product varchar(100) not null
);