from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.hooks.base import BaseHook

from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.operators.python import PythonOperator
from airflow.operators.empty import EmptyOperator

import pandas as pd
import json
from os import path
from sqlalchemy import create_engine

DB_NAME = 'Conn_Id_from_Airflow'


def _read_url_file(file_name: str) -> str:
    """
    Функция для чтения файлов
    Вход: принимает файлы с расширением xlsx, csv, sql
    Выход: если расширение xlsx, csv - адрес, если sql - содержимое файла
    """
    file_name = file_name.replace(r"\\", "/").replace("\\", "/")
    name = path.basename(file_name)

    xlsx_file: bool = name.lower().endswith('.xlsx')
    csv_file: bool = name.lower().endswith('.csv')
    sql_file: bool = name.lower().endswith('.sql')
    if not xlsx_file and not csv_file and not sql_file:
        raise NameError(f"Ожидалось расширение файла (xlsx, csv, sql),\
            получено: {path.splitext(name)[1]}")

    cur_dir = path.dirname(path.realpath(__file__))
    cur_dir = path.abspath(path.join(cur_dir, '..'))
    full_path = path.join(cur_dir, file_name)

    if not path.isfile(full_path):
        raise FileNotFoundError(
            f"Путь {full_path} не существует или не является файлом")

    if sql_file:
        with open(full_path, 'r', encoding="utf-8") as query_file:
            query = query_file.read()
            query_file.close()
        return query
    else:
        return full_path


def _read_article_move() -> json:
    """
    Чтение файла в DataFrame и преобразование JSON
    Выход: JSON
    """
    url = _read_url_file(r"data_insert/ДвижениеТоваров.xlsx")
    df = pd.read_excel(url)
    df.columns = ['code', 'material', 'material_name', 'batch', 'factory',
                  'warehouse', 'type_moving', 'type_product',
                  'material_doc', 'posit_product', 'date_moving',
                  'sum_product', 'count_product', 'type_count_product',
                  'main_tool', 'link_product']
    return df.to_json(orient='columns', date_unit='s', date_format='iso')


def _read_store() -> json:
    """
    Чтение файла в DataFrame и преобразование JSON
    Выход: JSON
    """
    url = _read_url_file(r"data_insert/Запасы.xlsx")
    df = pd.read_excel(url)
    df.columns = [
        'code_service', 'name_service', 'factory', 'warehouse', 'type_warehouse',
        'num_plane', 'name_plane', 'category', 'class_score', 'date_receipt',
        'mean_price', 'count_product', 'base_store', 'all_cost', 'material',
        'batch', 'product_elem', 'material_name', 'date_plane', 'date_receipt_mol',
        'type_receipt', 'group_party', 'name_group_material', 'responsible',
        'responsible_fact', 'line_product', 'name_line_activity', 'class_material',
        'class_name', 'type_reserve', 'finance_type', 'date_save', 'type_score',
        'main_account', 'weight_score', 'type_material', 'status_spp', 'currency',
        'price', 'price_more_yers', 'type_contractor', 'be', 'number_party',
        'name_contractor', 'name_sm', 'number_contract', 'type_ter',
        'date_contract', 'period_warrenty', 'free_reserve', 'group_sint',
        'name_group_sint', 'date_max', 'story_spp', 'order_client',
        'count_order_client', 'type_order_client', 'name_type_order_client',
        'order_price', 'last_order_price', 'okpd2', 'tnved',
        'internal_number_rnpt', 'external_number_rnpt', 'country_product'
    ]
    return df.to_json(orient='columns', date_unit='s', date_format='iso')


def push_article_move(**kwargs) -> None:
    """
    Функция записывает в XCom
    """
    ti = kwargs['ti']
    ti.xcom_push("article_move_data", _read_article_move())


def push_store(**kwargs) -> None:
    """
    Функция записывает в XCom
    """
    ti = kwargs['ti']
    ti.xcom_push("store_data", _read_store())


def pull_article_move(ti) -> None:
    """
    Функция получает данные из XCom и записываем в БД
    """
    result = ti.xcom_pull(task_ids='read_article_move', key='article_move_data')
    df = pd.read_json(result, orient='columns', convert_dates=True, date_unit='s')
    if df.empty:
        raise NameError("Получить данные из XCOM не удалось!")

    conn = BaseHook.get_connection(DB_NAME)
    con_str = f"postgresql+psycopg2://{conn.login}:{conn.password}@{conn.host}:{conn.port}/{conn.schema}"
    engine = create_engine(con_str)
    df.to_sql(name='article_mov', con=engine, schema='tmp', if_exists='append', index=False, method='multi')


def pull_store(ti) -> None:
    """
    Функция получает данные из XCom и записываем в БД
    """
    result = ti.xcom_pull(task_ids='read_store', key='store_data')
    df = pd.read_json(result, orient='columns', convert_dates=True, date_unit='s')
    if df.empty:
        raise NameError("Получить данные из XCOM не удалось!")

    conn = BaseHook.get_connection(DB_NAME)
    con_str = f"postgresql+psycopg2://{conn.login}:{conn.password}@{conn.host}:{conn.port}/{conn.schema}"
    engine = create_engine(con_str)
    df.to_sql(name='store', con=engine, schema='tmp', if_exists='append', index=False, method='multi')


def query_truncate_tmp() -> str:
    """Получение строки запроса"""
    return _read_url_file(r"data_insert/truncate_tmp.sql")


def query_create_func() -> str:
    """Получение строки запроса"""
    return _read_url_file(r"data_insert/func_for_trigger.sql")


def query_create_article_move() -> str:
    """Получение строки запроса"""
    return _read_url_file(r"data_insert/create_article_move.sql")


def query_create_store() -> str:
    """Получение строки запроса"""
    return _read_url_file(r"data_insert/create_store.sql")


def query_trun_article_move() -> str:
    """Получение строки запроса"""
    return _read_url_file(r"data_insert/trunsaction_article_move.sql")


def query_trun_store() -> str:
    """Получение строки запроса"""
    return _read_url_file(r"data_insert/trunsaction_store.sql")


default_args = {
    'owner': 'viktor',
    'depends_on_past': False,
    'email': ['vitboxt@mail.ru'],
    'email_on_failure': False,
    'email_on_retry': False
}

with DAG(
    dag_id="insert_dag_postgres",
    default_args=default_args,
    description='Перемещение данных из файла',
    schedule_interval=None,
    start_date=days_ago(1),
    tags=['Чтение из xlsx, csv и sql файлов'],
    catchup=False
) as dag:
    create_start = EmptyOperator(task_id='start_works_in_db')
    create_finish = EmptyOperator(task_id='finish_works_in_db')

    read_start = EmptyOperator(task_id='start_read_file')
    read_finish = EmptyOperator(task_id='finish_read_file')

    insert_start = EmptyOperator(task_id='start_insert_data_in_db')
    insert_level = EmptyOperator(task_id='level_insert')
    insert_finish = EmptyOperator(task_id='finish_insert_data_in_db')

    create_func_tr = PostgresOperator(
        task_id='function_for_trigger',
        postgres_conn_id=DB_NAME,
        sql=query_create_func()
    )
    create_am = PostgresOperator(
        task_id='create_table_article_move',
        postgres_conn_id=DB_NAME,
        sql=query_create_article_move()
    )
    create_st = PostgresOperator(
        task_id='create_table_store',
        postgres_conn_id=DB_NAME,
        sql=query_create_store()
    )
    read_article_move_xpush = PythonOperator(
        task_id='read_article_move',
        python_callable=push_article_move
    )
    read_store_xpush = PythonOperator(
        task_id='read_store',
        python_callable=push_store
    )
    trunc_tmp = PostgresOperator(
        task_id='truncate_tmp',
        postgres_conn_id=DB_NAME,
        sql=query_truncate_tmp()
    )
    insert_article_move_xpull = PythonOperator(
        task_id='insert_article_move_tmp',
        python_callable=pull_article_move
    )
    insert_store_xpull = PythonOperator(
        task_id='insert_store_tmp',
        python_callable=pull_store
    )
    trun_article_move = PostgresOperator(
        task_id='trans_article_move',
        postgres_conn_id=DB_NAME,
        sql=query_trun_article_move()
    )
    trun_store = PostgresOperator(
        task_id='trans_store',
        postgres_conn_id=DB_NAME,
        sql=query_trun_store()
    )

    create_start >> create_func_tr >> [create_am, create_st] >> create_finish \
        >> read_start >> [read_article_move_xpush, read_store_xpush] >> read_finish \
        >> insert_start >> trunc_tmp >> [insert_article_move_xpull, insert_store_xpull] \
        >> insert_level >> [trun_article_move, trun_store] >> insert_finish
